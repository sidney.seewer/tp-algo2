package s01;

import s01.BTree.BTNode;

import java.util.ArrayList;

public class BTreeItr {
  private BTree whole;
  // ------------------------------------------------------------
  private BTNode up   = null; 
  private BTNode down = null; 
  private boolean isLeftArc = false; // not relevant at the root
  // ------------------------------------------------------------
  /** Position on root. */
  public BTreeItr(BTree t) { 
    whole=t;down = whole.root;
  }
  // ------------------------------------------------------------
  // ------ Accessors
  // ------------------------------------------------------------
  /** Terminal position: no node above. */
  public boolean isRoot() {
    return up==null;
  }
  /** Position on a node with a left child. */
  public boolean hasLeft() { 
    return !isBottom() && down.left != null; // TODO - A ADAPTER...
  }
  /** Position on a node with a right child. */
  public boolean hasRight() {
    return !isBottom() && down.right != null; // TODO - A ADAPTER...
  }
  /** Terminal position: no node below. */
  public boolean isBottom() {
    return down==null;
  }
  /** Position on a node with no child. */
  public boolean isLeafNode() { 
    return !(isBottom()||hasLeft()||hasRight());
  }
  /** Position on a left child. Not relevant if isRoot(). */
  public boolean isLeftArc() {
    return isLeftArc;
  }
  /** PRE: !isBottom(). Returns the element stored at that position. */
  public Object consult() {
    assert (!isBottom());
    assert (isConsistent());
    return down.elt;
  }
  // ------------------------------------------------------------
  // ------ Movements
  // ------------------------------------------------------------
  /** The tree containing that position */
  public BTree whole() { 
    return whole;
  }
  /** PRE: !isRoot(). Does not move this itr but creates a new one.*/
  public BTreeItr up() {
    BTreeItr newItr = new BTreeItr(whole);
    newItr.down = up;
    newItr.up = up.parent;
    if(!newItr.isRoot()) {
      newItr.isLeftArc = up.parent.left == up;
    }else{
      newItr.isLeftArc = false;
    }
    return newItr; // TODO - A ADAPTER...
  }
  /** PRE: !isBottom(). Does not move this itr but creates a new one. */
  public BTreeItr left()      {
    assert (!isBottom());
    BTreeItr r = new BTreeItr(whole);
    r.up=down; r.down=down.left; r.isLeftArc = true;
    return r;
  }
  /** PRE: !isBottom(). Does not move this itr but creates a new one.  */
  public BTreeItr right()     { 
    assert (!isBottom());
    BTreeItr r = new BTreeItr(whole);
    r.up=down; r.down=down.right; r.isLeftArc = false;
    return r;
  }
  /** Goes down to the left as far as possible, from here. Always returns
   *  a bottom arc. Does not move this itr but creates a new one.  */
  public BTreeItr leftMost()  { 
    BTreeItr r = new BTreeItr(whole);
    r.up=up; r.down=down; r.isLeftArc = isLeftArc;
    while (!r.isBottom()) {
      r.up=r.down; r.down=r.down.left; r.isLeftArc = true;
    } 
    return r;
  }
  /** Goes down to the right as far as possible, from here. Always returns
   *  a bottom arc. Does not move this itr but creates a new one.  */
  public BTreeItr rightMost() {
    BTreeItr r = new BTreeItr(whole);
    r.up=up; r.down=down; r.isLeftArc = isLeftArc;
    while (!r.isBottom()) {
      r.up=r.down; r.down=r.down.right; r.isLeftArc = false;
    } 
    return r;    
  }
  //------------------------------------------------------------
  /** Returns a new itr on the same position. */
  public BTreeItr alias() {
    BTreeItr t = new BTreeItr(whole);
    t.whole = whole; t.up=up; t.down=down; t.isLeftArc=isLeftArc;
    return t;
  }
  //------------------------------------------------------------
  /** Each position belongs to exactly one tree. */
  public boolean isInside(BTree t) { 
    return whole==t;
  }
  // ------------------------------------------------------------
  // ------ Modifiers
  // ------------------------------------------------------------
  /** PRE: !isBottom(). Replaces the element stored at that position. */
  public void  update(Object elt) {
    this.down.elt = elt;
  }
  //------------------------------------------------------------
  /** Replaces the subtree with a single node containing that element. */
  public void insert(Object elt) {
    cut(); // TODO - A ADAPTER...
    if(up == null){
      BTNode node =  new BTNode(elt, null, null, null);
      whole.root = node;
      down = node;
    }else {
      if (isLeftArc()) {
        up.left = new BTNode(elt, null, null, up);
      } else {
        up.right = new BTNode(elt, null, null, up);
      }
    }
  }
  //------------------------------------------------------------
  /** Replaces the subtree with that whole tree. 
   * PRE: !this.isInside(t). POST: t is now empty.*/
  public void paste(BTree t) {
    if (isInside(t)) throw new IllegalArgumentException();
    cut();
    if (t.isEmpty()) return;
    BTNode n = t.root; 
    n.parent=up;
    if (isRoot())
      whole.root=n;
    else {
      if (isLeftArc) up.left = n;
      else           up.right= n;
    }
    down = n;  
    t.root = null; 
  }
  //------------------------------------------------------------
  /** Removes the subtree and returns it as a new tree. */
  public BTree cut() {
    // TODO - A COMPLETER...
    BTree newTree = new BTree();
    newTree.root = down;
    if(isRoot()) {
      whole.root = null;
      down = null;
    }else {
      if (!isBottom()) {
        down.parent = null;
      }
      if (isLeftArc) {
        up.left = null;
      } else {
        up.right = null;
      }
    }

    return newTree;
  }
  //------------------------------------------------------------
  @Override public String toString() {
    return whole.toString();
  }
  
  /** A nice representation of the whole tree */
  public String toReadableString() {
    return whole.toReadableString();
  }
  //------------------------------------------------------------
  //------ Non-public methods
  //------------------------------------------------------------
  // isConsistent() : tracks tree inconsistency (due to recent 
  //       PRE-conditions violations for cut/paste/insert) :
  //       - local bidirectional chaining
  //       - recursively, upwards, till the root arc
  //       This slows a lot, but may help detect bugs 
  // ------------------------------------------------------------
  private boolean isConsistent() {
    BTNode u=up, d=down;
    if (u!=null && d!=((isLeftArc)?(u.left):(u.right))) return false;
    while (u!=null) {
      if (d!=null && d.parent!=u) return false;
      if (d!=u.left && d!=u.right) return false;
      d=u; u=u.parent;
    }
    return (d==whole.root);
  }

}
