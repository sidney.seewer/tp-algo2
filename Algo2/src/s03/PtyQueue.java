package s03;

public class PtyQueue<E, P extends Comparable<P>> {
  private final Heap<HeapElt> heap;
  // ------------------------------------------------------------
  public PtyQueue() {
    heap = new Heap<HeapElt>(); // TODO - A COMPLETER
  }
  // ------------------------------------------------------------
  public boolean isEmpty() {
    return heap.isEmpty();
  }

  /** Adds an element elt with priority pty */ 
  public void enqueue(E elt, P pty) {
    heap.add(new HeapElt(pty, elt));
  }
  
  /** Returns the element with highest priority. PRE: !isEmpty() */ 
  public E consult() {
    return heap.min().elt; // TODO - A COMPLETER
  }

  /** Returns the priority of the element with highest priority. 
   *  PRE: !isEmpty() */ 
  public P consultPty() {
    return heap.min().pty; // TODO - A COMPLETER
  }
  
  /** Removes and returns the element with highest priority. 
   *  PRE: !isEmpty() */ 
  public E dequeue() {
    return heap.removeMin().elt; // TODO - A COMPLETER
  }

  @Override public String toString() {
    return heap.toString(); 
  }
  //=============================================================
  class HeapElt implements Comparable<HeapElt> {
    // TODO A COMPLETER
    private P pty;
    private E elt;
    
    public HeapElt(P thePty, E theElt) {
      // TODO A COMPLETER
      this.pty = thePty;
      this.elt = theElt;
    }

    @Override public int compareTo(HeapElt arg0) {
      return this.pty.compareTo(arg0.pty); // TODO A COMPLETER
    }
  }
}

