S15 - Ex. 1 - N-Queens Problem
==============================
To visualize the behaviour of your algorithm, the queensfx_s15.jar 
library has to be imported (and it uses JavaFX). 
With IntelliJ: File -> Project Structure -> Modules 
                     -> Dependencies -> + -> JARs or directory
                     -> queensfx_s15.jar

This component supposes that your source code is in a package named s15.
