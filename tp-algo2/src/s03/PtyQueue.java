package s03;

public class PtyQueue<E, P extends Comparable<P>> {
  private final Heap<HeapElt> heap;
  private static int index;
  // ------------------------------------------------------------
  public PtyQueue() {
    heap = new Heap<HeapElt>();
  }
  // ------------------------------------------------------------
  public boolean isEmpty() {
    return heap.isEmpty();
  }

  /** Adds an element elt with priority pty */
  public void enqueue(E elt, P pty) {
    heap.add(new HeapElt(pty, elt, index++));
  }

  /** Returns the element with highest priority. PRE: !isEmpty() */
  public E consult() {
    return heap.min().elt;
  }

  /** Returns the priority of the element with highest priority.
   *  PRE: !isEmpty() */
  public P consultPty() {
    return heap.min().pty;
  }

  /** Removes and returns the element with highest priority.
   *  PRE: !isEmpty() */
  public E dequeue() {
    return heap.removeMin().elt;
  }

  @Override public String toString() {
    return heap.toString();
  }
  //=============================================================
  class HeapElt implements Comparable<HeapElt> {
    public final P pty;
    public final E elt;
    int order;


    public HeapElt(P thePty, E theElt, int order) {
      pty = thePty;
      elt = theElt;
      this.order = order;
    }

    @Override public int compareTo(HeapElt arg0) {
      int r = this.pty.compareTo(arg0.pty);

      if(r == 0) {

        return this.order > arg0.order ? 1 : -1;
      } else {
        return r;

      }
    }
  }
}

