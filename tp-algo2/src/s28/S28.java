
package s28;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.*;
import java.math.*;

public class S28 {
  
  // ------ Ex. 1 -----------
  public static void printSomePowersOfTwo() {
    System.out.println("Power of two from numbers 0 to 10:");
    int[] numbers = {0,1,2,3,4,5,6,7,8,9,10};
    Arrays.stream(numbers)
          .map(e -> (int)Math.pow(2,e))
          .forEach(System.out::println);
  }

    // ------ Ex. 2 -----------
  public static String macAddress(int[] t) {
    String macString = Arrays.stream(t)
                          .mapToObj(e -> String.format("%02X",e))
                          .collect(Collectors.joining(":"));
    return macString;
  }
  
  // ------ Ex. 3 -----------

  public static long[] divisors(long n) {
    return LongStream.range(1,n-1)
                     .filter(e -> (n % e) == 0)
                     .toArray();
  }

  // ------ Ex. 4a -----------
  public static long sumOfDivisors(long n) {
    //System.out.println("Sum of divisors of "+n+": ");
    return Arrays.stream(divisors(n)).sum();
  }
  
  
  // ------ Ex. 4b -----------
  public static long[] perfectNumbers(long max) {
    return LongStream.range(1,max-1)
                     .filter(e-> e==sumOfDivisors(e))
                     .toArray();
  }
  
  // ------ Ex. 5 -----------
  public static void printMagicWord() throws IOException {
    Path path = FileSystems.getDefault().getPath("D:\\_HEIA\\2. Jahr\\3. Semester\\Algo\\TP_Git\\tp-algo2\\src\\s28\\wordlist.txt");
    Files.lines(path)
      .filter(x -> x.length() == 11)
      .filter(x -> x.charAt(2) == 't')
      .filter(x -> x.charAt(4) == 'l')
      .filter(x -> x.chars().distinct().count() == 6)
      .forEach(System.out::println);
   }
  
  public static boolean isPalindrome(String str) {
    return str.equals(new StringBuilder(str).reverse().toString());
  }

  public static void printPalindromes() throws IOException {
    Path path = FileSystems.getDefault().getPath("D:\\_HEIA\\2. Jahr\\3. Semester\\Algo\\TP_Git\\tp-algo2\\src\\s28\\wordlist.txt");
    Files.lines(path)
         .filter(x->x.length()>=4)
         .filter(S28::isPalindrome)
         .forEach(System.out::println);
  }

  // ------ Ex. 6 -----------
  public static void averages() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = new double[results.length];
    for (int i = 0; i < results.length; i++) {
        double sum = 0;
        for (int j = 0; j < results[i].length; j++) {
            sum += results[i][j];
        }
        grades[i] = 1.0 + sum / 10.0;
    }
    double sum = 0;
    for (int i = 0; i < grades.length; i++) {
        sum += grades[i];
    }
    double average = sum / grades.length;

    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  public static void averagesWithStreams() {
    double basePoint = 1.0;
    double divisor = 10.0;
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = Arrays.stream(results)
            .mapToDouble(e -> DoubleStream.of(e).sum())
            .map(e -> basePoint + e/divisor)
            .toArray();
    double average = DoubleStream
            .of(grades)
            .average()
            .getAsDouble();

    System.out.println("Averages with streams:");
    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  // ------ Ex. 8 -----------

  static DoubleStream sampling(double from, double to, int nSubSamples) {
    Random r = new Random();
    return DoubleStream.builder().build()
                .limit(nSubSamples)
                .boxed().mapToDouble(e -> from + (to - from) * r.nextDouble());
    // TODO ...
  }
  
  public static void ex8() {
    // calculate product
    double[] product = {1.2,3.4,5.6};
    System.out.println(Arrays.stream(product).reduce(1, (a, b) -> a*b));
    // Calculate series
    System.out.println(LongStream.range(0,20).mapToDouble(e -> (e / Math.pow(2,e))).sum());
    // Concatenation of Strings
    String[] hello = {"Hello","World","!"};
    System.out.println(String.join(" ", hello));
    // Estimate max of function
    System.out.println(sampling(0, Math.PI, 1002).map(x -> Math.sin(x)*Math.sin(x)*
            Math.cos(x)).max());
  }

  // ------ Ex. 9 -----------
  public static void nipas(int n) {
    System.out.println(
      IntStream.range(0, n)
      .mapToObj(
        i -> IntStream.range(i, i+4)
             .mapToObj(j ->
               new String(new char[n+2-j]).replace("\0", " ") +
               new String(new char[1+2*j]).replace("\0", "*"))
             .collect(Collectors.joining("\n")))
      .collect(Collectors.joining("\n"))
    );
  }

  //-------------------------------------------------------
  public static void main(String[] args) {
    //printSomePowersOfTwo();

    //System.out.println("\nNew Mac adress after transformation: "+macAddress(new int[]{78, 26, 253, 6, 240, 13}));

    //System.out.println(Arrays.toString(divisors(496L)));

    //System.out.println(sumOfDivisors(496L));

    //System.out.println(Arrays.toString(perfectNumbers(10_000)));

    /*
    try {
      printMagicWord();
      printPalindromes();
    } catch(IOException e) {
      System.out.println("!! Problem when reading file... "+e.getMessage());
    }

    averages();
    averagesWithStreams();
    */
    ex8();
    // nipas(4); // read/analyze the code first!...

  }

}
