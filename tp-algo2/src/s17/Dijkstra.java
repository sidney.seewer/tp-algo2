package s17;
import s03.PtyQueue;

import java.util.*;

// ------------------------------------------------------------
public class Dijkstra {
  // ------------------------------------------------------------
  static class Vertex implements Comparable<Vertex> {
    public final int vid;
    public final int pty;
    public Vertex(int vid, int pty) {
      this.vid=vid; this.pty=pty;
    }
    @Override public int compareTo(Vertex v) {
      return Integer.compare(this.pty,  v.pty);
    }
  }
  // ------------------------------------------------------------
  // POST : minDist[i] is the min distance from a to i
  //                      MAX_VALUE if i is not reachable, 0 for a
  //         parent[i] is the parent of i in the corresponding tree
  //                      -1 if i is not reachable, and for a
  public static void dijkstra(WeightedDiGraph g, int a, 
                              int[] minDist, int[] parent) {

    boolean[] isVisited = new boolean[minDist.length];
    Arrays.fill(minDist, Integer.MAX_VALUE);
    Arrays.fill(parent, -1);

    minDist[a] = 0;

    PtyQueue<Integer, Integer> pq = new PtyQueue();

    pq.enqueue(a, minDist[a]);

    while(!pq.isEmpty()){
      int k = pq.dequeue();
      if(isVisited[k]) continue;
      isVisited[k] = true;

      for(int i : g.neighboursFrom(k)){
        int dist = minDist[k] + g.edgeWeight(k, i);
        if(minDist[i] > dist) {
          minDist[i] = dist;
          parent[i] = k;
        }
        pq.enqueue(i, minDist[i]);
      }
    }

    return;
  }

  public static void dijkstraInv(WeightedDiGraph g, int a, int[] minDist, int[] parent) {
    // TODO - Done ...

    boolean[] isVisited = new boolean[minDist.length];
    Arrays.fill(minDist, Integer.MAX_VALUE);
    Arrays.fill(parent, -1);

    minDist[a] = 0;

    PtyQueue<Integer, Integer> pq = new PtyQueue();

    pq.enqueue(a, minDist[a]);

    while (!pq.isEmpty()){
      int k = pq.dequeue();
      if(isVisited[k])continue;
      isVisited[k] = true;

      for(int i : g.neighboursTo(k)){ // CHANGES
        int dist = minDist[k] + g.edgeWeight(i, k); // CHANGES
        if(minDist[i] > dist){
          minDist[i] = dist;
          parent[i] = k;
        }
        pq.enqueue(i, minDist[i]);
      }
    }
  }

  // ------------------------------------------------------------
  /** returns all vertices in vid's strongly connected component */
  static Set<Integer> strongComponentOf(WeightedDiGraph g, int vid) {
    Set<Integer> res=new HashSet<>();

    res.add(vid);
    int nbNodes = g.nbOfVertices();

    int[] from = new int[nbNodes];
    int[] to = new int[nbNodes];

    dijkstra(g, vid, new int[nbNodes], from);
    dijkstraInv(g, vid, new int[nbNodes], to);

    for (int i = 0; i < nbNodes; i++) {
      if(from[i] != -1 && to[i] != -1) res.add(i);
    }
    return res;
  }

  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6; // int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int[] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int[] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int[] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};

    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("Input Graph: " + g);

    int n = g.nbOfVertices();
    int[] minCost = new int[n];
    int[] parent  = new int[n];
    for (int a=0; a<n; a++) {
      dijkstra(g, a, minCost, parent);
      System.out.println("\nMinimal distances from " +a); 
      for (int i=0; i<minCost.length; i++) {
        String s="to "+ i +":";
        if (minCost[i]==Integer.MAX_VALUE) s+=" unreachable";
        else s+= " total " + minCost[i] +", parent "+parent[i];
        System.out.println(s);
      }
    }
  }
}
