package s05;

public class FIFO_pile<E> {
    int next_in;
    int next_out;
    E table[];
    public FIFO_pile(){
        this.next_out = 0;
        this.next_in = 0;
        //Define table start Size
    }
    private boolean is_empty(){
        return table.length==0;
    }
    public void add(E elt){
        //Duplicate table if there is no space left
        table[next_in]=elt;
        next_in++;
    }
    public E pop() throws Exception{
        if(is_empty())
            throw new Exception("Pile is empty");
        E e_pop = table[next_out];
        next_out++;
        return e_pop;
    }
    public void print_table_int(){
        for(int i = 0; i<this.table.length;i++){
            System.out.printf("%d, ",(int)this.table[i]);
        }
        System.out.println();
    }
}
