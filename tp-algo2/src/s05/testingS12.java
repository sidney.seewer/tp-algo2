package s05;

public class testingS12 {
    public static int[] rotate(int[] t, int i){
        //Copy array
        int[] result = new int[t.length];
        for(int h = 0; h<t.length;h++){
            result[h] = t[h];
        }
        int counter = 0;
        for(int k = i; k < t.length; k++){
            result[counter]=t[k];
            counter++;
        }
        for(int j = 0; j<i;j++){
            result[counter+j]=t[j];
        }
        return result;
    }
    public static void find_anagramms(String[] t,int i){
        String oj_word = t[i];
        boolean[] taken = new boolean[oj_word.length()];
        boolean is_match = true;
        //Go through the array of words
        for(int j = 0; j<t.length;j++){
            //For each word
            for(int k = 0; k<t[j].length();k++){
                //For each letter in the word see if it is in the wanted word (oj_word)
                for(int h = 0; h<oj_word.length();h++){
                    if(oj_word.charAt(h)==t[j].charAt(k)){
                        if(!taken[h]){
                            taken[h]=true;
                        }
                    }
                }
            }
            //Check if every letter is taken and that the number of letters is the same
            for(boolean ch : taken){
                if(!ch){
                    is_match=false;
                }
            }
            if(oj_word.length()==t[j].length()&&is_match){
                //Print word
                for(int f = 0; f<t[j].length();f++){
                    System.out.printf("%c",t[j].charAt(f));
                }
                System.out.println();
            }
            //Reset taken table and is_match
            is_match=true;
            for(boolean ch:taken){
                ch=false;
            }
        }
    }
    public static void main(String[] args) {
        //Find anagramms
        String[] s = {"spot","zut","pots","stop","hello"};
        find_anagramms(s,2);

        //Rotate Table
        System.out.println("Rotate Table:");
        int[] t = {0,1,2,3,4,5,6};
        System.out.println("Before:");
        pArr(t);
        int[] r = rotate(t,3);
        System.out.println("After:");
        pArr(r);
        System.out.println();

    }
    public static void pArr(int[] r){
        for(int i= 0; i < r.length;i++){
            System.out.printf("%d, ",r[i]);
        }
        System.out.println();
    }
}
