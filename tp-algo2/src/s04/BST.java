package s04;
// =======================
public class BST<E extends Comparable<E>> {
  protected BTree<E> tree;
  protected int   crtSize;

  public BST() {
    tree = new BTree<E>();
    crtSize = 0;
  }

  public BST(E [] tab) {  // PRE sorted, no duplicate
    // TODO - A COMPLETER
    tree = optimalBST(tab,0, tab.length-1);
    crtSize = 0;
  }

  /** returns where e is, or where it should be inserted as a leaf */
  protected  BTreeItr<E> locate(E e) {
    BTreeItr<E> itr = tree.root();
    while(!itr.isBottom() && itr.consult().compareTo(e) != 0) {
      if (itr.consult().compareTo(e) == 0) {
        break;
      }else{
        itr = itr.consult().compareTo(e) > 0 ? itr.left() : itr.right();
      }
    }
    return itr; // TODO - A COMPLETER
  }

  public void add(E e) {
    // TODO - A COMPLETER
    BTreeItr<E> itr = locate(e);
    if(itr.isBottom()){
      itr.insert(e);
      crtSize++;
    }
  }

  public void remove(E e) {
    BTreeItr<E> itr = locate(e);
    if(!itr.isBottom()) {
      while (itr.hasRight()) {
        itr.rotateLeft();
        itr = itr.left();
      }
      BTree<E> tr = itr.cut();
      if (tr.root().hasLeft()) {
        itr.paste(tr.root().left().cut());
      }
      crtSize--;
    }

    // TODO - A COMPLETER
  }

  public boolean contains(E e) {
    BTreeItr<E> ti = locate(e);
    return ! ti.isBottom();
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public E minElt() {
    BTreeItr<E> itr = tree.root();
    return itr.leftMost().consult();  // TODO - A COMPLETER
  }

  public E maxElt() {
    BTreeItr<E> itr = tree.root();
    return itr.rightMost().consult(); // TODO - A COMPLETER
  }

  @Override public String toString() {
    return ""+tree;
  }
  
  public String toReadableString() {
    String s = tree.toReadableString();
    s += "size=="+crtSize+"\n";
    return s;
  }
  
  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  
  private BTree<E> optimalBST(E[] sorted, int left, int right) {
    BTree<E> r = new BTree<E>();
    BTreeItr<E> ri = r.root();
    // TODO - A COMPLETER
    ri.insert(sorted[(left + right)/2]);
    ri.left();
    optimalBST(ri, sorted, left, (left + right)/2);
    ri = r.root().right();
    optimalBST(ri, sorted, (left+right)/2, right);
    return r;
  }
  private void optimalBST(BTreeItr<E> itr, E[] tab, int left, int right){
  }
}
