package s04;

public class S12 {
    public static void main(String[] args) {
        String str = "aab";
        System.out.printf("%s\n",e08_find_max_duplicate(str));
        int[][] matrix = {
                {1,3,2,4},
                {2,8,1,3},
                {2,5,3,2},
                {4,4,4,8}};
        propagate(matrix,8);
    }
    static String e08_find_max_duplicate(String s){
        String dup="";
        /*
        Pseudo Code:
        For the first to the second last letter:
            Find the same letter in the rest of the string
            If the same letter is found
                Check the next index for both and see if they match, save the longest chain
         */
        int temp_index1, temp_index2;
        int best_index1 = -1;
        int best_len = 0;
        int counter = 0;
        for(int i = 0; i < s.length()-1;i++){
            temp_index1 = i;
            for(int j = i+1; j<s.length();j++){
                if(s.charAt(j)==s.charAt(temp_index1)){
                    temp_index2 = j;
                    counter=1;
                    while(temp_index2+counter < s.length() &&s.charAt(temp_index1+counter)==s.charAt(temp_index2+counter)){
                        counter++;
                    }
                    if(counter > best_len){
                        best_len = counter;
                        best_index1 = temp_index1;
                    }
                    counter = 0;
                }
            }
        }
        if(best_index1==-1){
            return "There is no duplication";
        }
        for(int k = 0; k < best_len;k++){
            dup = s.substring(best_index1,best_index1+best_len);
        }
        return dup;
    }
    static void propagate(int[][] t, int v){
        int counter = 0;
        //Go through the table diagonally
        while(!(counter >= t.length && counter >= t[0].length)){
            for(int i = 0; i < t.length;i++){
                if(t[i][0]==v){
                    
                }
            }
            counter ++;
        }
    }
}
