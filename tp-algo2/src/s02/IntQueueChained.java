package s02;
public class IntQueueChained {
  //======================================================================
  /* TODO: adapt using pseudo-pointers instead of queue node objects
   * "Memory management" code:
   * - define "memory" arrays, the NIL constant, and firstFreeCell
   * - define allocate/deallocate, with automatic array expansion
   * "User" code:
   * - modify enqueue/dequeue/..., keeping the same logic/algorithm
   * - test
   */

  private static final int NIL = -1;
  private static int size = 10;
  private static int[] elts = new int[size];
  private static int[] next = new int[size];
  private static int firstFreeCell = 0;
  static{
    for (int i = 0; i < size-1; i++) {
      next[i] = i+1;
    }
    next[size-1] = NIL;
  }

  private static void doubleMem(){
    int[] newElts = new int[size*2];
    int[] newNexts = new int[size*2];
    System.arraycopy(elts, 0, newElts, 0, size);
    System.arraycopy(next, 0, newNexts, 0, size);
    firstFreeCell = size;
    for (int i = size; i < size*2-1; i++) {
      newNexts[i] = i+1;
    }
    size *= 2;
    newNexts[size-1] = NIL;
    elts = newElts;
    next = newNexts;
  }

  private static int allocate(int before){
    if(firstFreeCell == NIL){
      doubleMem();
    }
    int index = firstFreeCell;
    firstFreeCell = next[index];
    if(before != NIL)next[before] = index;
    return index;
  }

  private static void deallocate(int index){
    elts[index] = 0;
    next[index] = firstFreeCell;
    firstFreeCell = index;
  }
  //======================================================================
  private int front = NIL;
  private int back = NIL;
  // ------------------------------
  public IntQueueChained() {}
  // --------------------------
  public void enqueue (int elt) {
    int i = allocate(back);
    if(back == NIL) {
      front = i;
    }
    back = i;
    elts[back] = elt;
  }
  // --------------------------
  public boolean isEmpty() {
    return back == NIL;
  }
  // --------------------------
  public int consult() {
    return elts[front];
  }
  // --------------------------
  public int dequeue() {
      int e = elts[front];
      int oldFront = front;
      if (front == back) {
        back = NIL;
        front = NIL;
      } else {
        front = next[front];
      }
    deallocate(oldFront);
    return e;
  }
}
