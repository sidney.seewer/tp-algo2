package tsp;

//======================================================================
public class TSP17 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j;
    int n = t.length;
    int closestPt = 0;
    double shortestDist;
    double dist;

    int nbVisited = 1;

    TSPPoint p1, p2;

    for (int k = 0; k < n; k++) {
      path[k] = k;
    }
    if (n <= 0) return;
    // Removing the useless calcul n-1 (already done)
    for(i=1; i<n; i++) {
      shortestDist = Double.MAX_VALUE;
      p1 = t[i-1];
      for(j=nbVisited; j<n; j++) {

        // Removing the 2 calls to the method distance into 1
        // Removing the calls to the method distance
        p2 = t[j];
        dist = Math.sqrt(sqr(p1.x-p2.x) + sqr(p1.y-p2.y));
        if (dist < shortestDist ) {
          shortestDist = dist;
          closestPt = j;
        }
      }

      TSPPoint temp = t[closestPt];
      t[closestPt] = t[nbVisited];
      t[nbVisited] = temp;
      nbVisited++;
    }
  }
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  }
