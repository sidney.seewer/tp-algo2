package tsp;
//======================================================================
public class TSP13 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j;
    int n = t.length;
    boolean[] visited = new boolean[n];
    int thisPt, closestPt = 0;
    double shortestDist;
    double dist; // Added to store the distance

    TSPPoint a, b;

    thisPt = n-1;
    if (thisPt < 0) return;
    visited[thisPt] = true;
    // Removing the useless calcul n-1 (already done)
    path[0] = thisPt;  // chose the starting city
    for(i=1; i<n; i++) {
      shortestDist = Double.MAX_VALUE;
      for(j=0; j<n; j++) {
        if (visited[j])
          continue;

        // Removing the 2 calls to the method distance into 1
        // Removing the calls to the method distance
        a = t[thisPt];
        b = t[j];
        dist = Math.sqrt(sqr(a.x-b.x) + sqr(a.y-b.y));
        if (dist < shortestDist ) {
          shortestDist = dist;
          closestPt = j;
        }
      }
      path[i] = closestPt;
      visited[closestPt] = true;
      thisPt = closestPt;
    }
  }
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  }
