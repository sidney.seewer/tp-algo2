package s10;
import java.util.Random;
//--------------------------------------------------
public class Treap<E extends Comparable<E>>  {
  //============================================================
  static class TreapElt<E extends Comparable<E>> implements Comparable<TreapElt<E>> {
    static Random rnd=new Random();
    // -----------------------
    private final E elt;
    private int     pty;
    // -----------------------
    public TreapElt(E e) {
      elt=e; 
      pty=rnd.nextInt();
    }

    public int pty() {
      return pty;
    }

    public E elt() {
      return elt;
    }

    public int compareTo(TreapElt<E> o) {
      return elt.compareTo(o.elt);
    }

    @Override public boolean equals(Object o) {
      if(o==null) return false;
      if (this.getClass() != o.getClass()) return false;
      if (elt==null) return false;
      return elt.equals(((TreapElt<?>)o).elt);
    }

    @Override public String toString() {
      return ""+elt+"#"+pty;
    }

    @Override public int hashCode() {
      return elt.hashCode();
    }
  }
  //============================================================
  private final BST<TreapElt<E>> bst;
  public int countAdd = 0;
  public int countRemove = 0;
  public int countRotationAdd = 0;
  public int countRotationRemove = 0;
  // --------------------------
  public Treap() {
    bst=new BST<>();
  }

  public void add(E e) {
    countAdd++;
    TreapElt<E> treapElt = new TreapElt<E>(e);
    bst.add(treapElt);
    BTreeItr<TreapElt<E>> itr = bst.locate(treapElt);
    percolateUp(itr);

  }

  public void remove(E e) {
    countRemove++;
    TreapElt<E> treapElt = new TreapElt<E>(e);
    if (!contains(e)) return;
    BTreeItr<TreapElt<E>> itr = bst.locate(treapElt);
    siftDownAndCut(itr);
  }

  public boolean contains(E e) {
    if(bst.isEmpty())
      return false;
    return bst.contains(new TreapElt<>(e));
  }

  public int size() {
    return bst.crtSize;
  }

  public E minElt() {
    return bst.minElt().elt;
  }

  public E maxElt() {
    return bst.maxElt().elt;
  }
  
  public String toString() {
    return bst.toString();
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  private void siftDownAndCut(BTreeItr<TreapElt<E>> ti) {
    // Do until leaf is reached
    while (!ti.isLeafNode()){
      countRotationRemove++;
      // if there are two sons
      if (ti.hasLeft() && ti.hasRight()){
        // If the left son has a lower priority than the right one
        if (isLess(ti.left(), ti.right())){
          ti.rotateRight();
          ti = ti.right();
        }
        // If the right son has a lower priority
        else {
          ti.rotateLeft();
          ti = ti.left();
        }
      } // If there is only one son
      // If this son is left
      else if(ti.hasLeft()){
        ti.rotateRight();
        ti = ti.right();
      }
      // If this son is right
      else {
        ti.rotateLeft();
        ti = ti.left();
      }
    }
    bst.remove(ti.consult());
  }

  private void percolateUp(BTreeItr<TreapElt<E>> ti) {
    while((!ti.isRoot()) && isLess(ti, ti.up())) {
      countRotationAdd++;
      if (ti.isLeftArc()) {ti=ti.up(); ti.rotateRight();}
      else                {ti=ti.up(); ti.rotateLeft(); }
    }
  }

  private boolean isLess(BTreeItr<TreapElt<E>> a, BTreeItr<TreapElt<E>> b) {
    TreapElt<E> ca= a.consult();
    TreapElt<E> cb= b.consult();
    return ca.pty()<cb.pty();
  }
  public void printStats(){
    System.out.printf("Add \t%d\n",countAdd);
    System.out.printf("Remove \t%d\n",countRemove);
    System.out.printf("RotAdd \t%d\n",countRotationAdd);
    System.out.printf("RotRem \t%d\n",countRotationRemove);
    System.out.printf("Average of Rotation add:\t%f\n",(double)countRotationAdd/(double)countAdd);
    System.out.printf("Average of Rotation remove:\t%f\n",(double)countRotationRemove/(double)countRemove);
  }
}
