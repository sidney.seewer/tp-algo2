package s11;

import org.junit.Test;
import org.junit.runners.JUnit4;
import org.openjdk.jmh.util.ListStatistics;

import static org.junit.Assert.*;

public class IntStackTest {
    int[] testElts = {-3,-2,-1,0,1,2,3};
    @Test
    public void testInstStack(){
        IntStack iStack = new IntStack();
        //Test if empty by creation
        if(!iStack.isEmpty()){
            System.out.println("Not empty upon creation. Error in isEmpty");
            return;
        }
        iStack.push(testElts[0]);
        if(iStack.isEmpty()){
            System.out.println("Still empty after adding an elt. Error in push");
            return;
        }
        if(iStack.top()!=testElts[0]){
            System.out.println("Added elt is not top elt. Error in push or pull");
            return;
        }
        iStack.pop();
        if(!iStack.isEmpty()){
            System.out.println("Stack is not empty after adding and removing elt. Error in push or pull");
            return;
        }
        iStack.push(testElts[1]);
        if(iStack.top()!=testElts[1]){
            System.out.println("Added elt is not on top. Error in push or top");
            return;
        }
        if(!testDoubleSize()){
            System.out.println("Error in doubling the size");
            return;
        }
        System.out.println("Passed all test");
    }
    public boolean testDoubleSize(){
        IntStack iStack= new IntStack(2);
        for (int testElt : testElts) {
            iStack.push(testElt);
        }
        if(iStack.top()!=testElts[testElts.length-1]){
            System.out.println("Top element is not the last inserted one");
            return false;
        }
        for(int i = 0; i< testElts.length;i++){
            iStack.pop();
        }
        if(!iStack.isEmpty()){
            System.out.println("Stack is not empty after removing as many elemtns as were added");
            return false;
        }
        return true;
    }
}