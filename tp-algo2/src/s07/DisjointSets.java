/*
private final int[] parents;
private int nbSets;

public DisjointSets(int nbOfElements) {
    parents = new int[nbOfElements];
    nbSets = nbOfElements;
    for (int i = 0; i < parents.length; i++) {
        parents[i] = -1;
    }
}

// return index of root
private int optimizePathToRoot(int i) {
    assert (i != parents[i]);
    if (parents[i] < 0) return i;
    int root = optimizePathToRoot(parents[i]);
    parents[i] = root;
    return root;
}

public boolean isInSame(int i, int j) {
    return optimizePathToRoot(i) == optimizePathToRoot(j);
}

public void union(int i, int j) {
    if (isInSame(i, j)) return;

    i = optimizePathToRoot(i);
    j = optimizePathToRoot(j);
    // if i has more elements than j
    if (parents[i] < parents[j]) {
        appendSubtree(i, j);
    } else {
        appendSubtree(j, i);
    }
    nbSets--;
}

public void appendSubtree(int root, int child){
    parents[root] += parents[child];
    parents[child] = root;
}

public int nbOfElements() {  // as given in the constructor
    return parents.length;
}

private void optimizeAllPaths(){
    for (int j = 0; j < parents.length; j++){
        optimizePathToRoot(j);
    }
}

// O(n ln) almost O(n)
public int minInSame(int i) {
    int min = i;
    for (int j = 0; j < parents.length; j++){
        if (j < min && isInSame(i, j)) min = j;
    }
    return min;
}

public boolean isUnique() {
    return nbSets == 1;
}

@Override
public String toString() {
    optimizeAllPaths();
    int[] roots = new int[nbSets];
    int index = 0;
    for (int i = 0; index < roots.length; i++){
        if (parents[i] < 0)
        roots[index++] = i;
    }

    StringJoiner global = new StringJoiner(",");
    StringJoiner local;
    for (int i = 0; i < roots.length; i++){
        local = new StringJoiner(",");
        for (int j = 0; j < parents.length; j++){
            if (j == roots[i] || parents[j] == roots[i]) local.add("" + j);
        }
        global.add("{" + local + "}");
    }

    return global.toString();
}

// O(n^2)
@Override
public boolean equals(Object otherDisjSets) {
    if (otherDisjSets == null || !(otherDisjSets instanceof DisjointSets)) return false;

    DisjointSets other = (DisjointSets)otherDisjSets;
    if (nbOfElements() != other.nbOfElements()) return false;

    for (int i = 0; i < parents.length; i++){
        for (int j = 0; j < parents.length; j++){
            if (isInSame(i, j) != other.isInSame(i, j)) return false;
        }
    }
    return true;
}

public static void main(String[] args){
    DisjointSets sets = new DisjointSets(15);
    System.out.println(sets.toString());
    sets.union(4,8);
    sets.union(2,3);
    sets.union(8, 5);
    sets.union(2, 12);
    sets.union(1, 14);
    System.out.println(sets.toString());
    System.out.println(sets.minInSame(8)); // 4
    System.out.println(sets.minInSame(14)); // 1
    System.out.println(sets.minInSame(2)); // 2
    System.out.println(sets.minInSame(0)); // 0
}

*/