package s13;

import java.io.*;

// Reads any file one bit at a time (most significant bit first)
public class BitReader {
  private final FileInputStream fis;
  private int val;
  private int ctr;
  private int isOver;

  public BitReader(String filename) throws IOException {
    fis= new FileInputStream(filename);
    val = fis.read();
  }

  public void close() throws IOException {
    fis.close();
  }

  public boolean next() throws IOException {
    if (ctr >= 8) {
      ctr = 0;
      val = isOver;
    } else if (ctr == 7) {
      isOver = fis.read();
    }

    int mask = (val & 0xff) >> 7;
    val = val << 1;
    ctr++;
    return mask == 1;
  }

  public boolean isOver() {
    return isOver == -1;
  }

  //-------------------------------------------
  // Tiny demo...
  public static void main(String [] args) {
    String filename="a.txt";
    try {
      BitReader b = new BitReader(filename);
      int i=0;
      while(!b.isOver()) {
        System.out.print(b.next()?"1":"0");
        i++;
        if (i%8  == 0) System.out.print(" ");
        if (i%80 == 0) System.out.println("");
      }
      b.close();
    } catch (IOException e) {
      throw new RuntimeException(""+e);
    }
  }
}
