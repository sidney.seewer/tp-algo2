package s09;
import java.math.BigInteger;
import java.math.BigInteger.*;

// ------------------------------------------------------------ 
public class StringSearching {
  // ------------------------------------------------------------
  //301 and 256
  static /*final*/ int HASHER = 301; // Maybe also try with 7 and 46237
  static /*final*/ int BASE   = 257; // Please also try with 257
  // ---------------------
  static int firstFootprint(String s, int len) {
    int footprint = 0;
    for (int i = 0; i < len; i++) {
      footprint += s.charAt(i) % HASHER;
      if (i < len-1){
        footprint = (footprint%HASHER * BASE%HASHER) % HASHER;
      }
    }
    return footprint % HASHER;
  }
  // ---------------------
  // must absolutely be O(1)
  // coef is (BASE  power  P.LENGTH-1)  mod  HASHER
  static int nextFootprint(int previousFootprint, char dropChar, char newChar, int coef) {
    int next_footprint = (((previousFootprint - dropChar * coef) % HASHER) + HASHER) % HASHER;
    next_footprint = (next_footprint * BASE) % HASHER;
    return (next_footprint + newChar) % HASHER;
  }
  // This function makes the modulo for each exponent with a certain base
  private static int modPow(int base, int exp, int mod) {
    int value = 1;
    while (exp-- > 0) {
      value *= base % mod;
      value %= mod;
    }
    return value;
  }
  // ---------------------
  // Rabin-Karp algorithm
  public static int indexOf_rk(String t, String p) {
    if (t.length() < p.length()) return -1;
    // Calculate the first Footprints
    int footprint_t = firstFootprint(t,p.length());
    int footprint_p = firstFootprint(p,p.length());
    int coef =modPow(BASE, p.length()-1, HASHER);
    int result = -1;
    for (int i = 0; i <= t.length()-p.length(); i++) {
      // If the footprints are the same
      if (footprint_t == footprint_p){
        String chain = t.substring(i,i+p.length());
        // If the strings are truly the same (worst case scenario)
        if (chain.equals(p)){
          result = i;
          break;
        }
      }
      if (i < t.length()-p.length()) {
        footprint_t = nextFootprint(footprint_t, t.charAt(i), t.charAt(i + p.length()), coef);
      }
    }
    return result;
  }

  public static void main(String[] args) {
    int[][] image = {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};
    int width = 5;
    int[][] rotimage = new int[width][width];
    //Print canvas
    System.out.println("Normal Table:");
    for(int i = 0; i<width;i++){
      for(int j= 0; j<width;j++){
        System.out.printf("%d ",image[i][j]);
      }
      System.out.println();
    }
    System.out.println();
    //Rotate the table into rotImage
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < width; j++) {
        rotimage[i][j]=image[width-1-j][i];
      }
    }
    //Print new image:
    System.out.println("Rotated 90 table:");
    for(int i = 0; i<width;i++){
      for(int j= 0; j<width;j++){
        System.out.printf("%d ",rotimage[i][j]);
      }
      System.out.println();
    }
    System.out.println();
    //Rotate the table 180 again
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < width; y++) {
        rotimage[x][y]=image[width-1-x][width-1-y];
      }
    }
    //Print new image:
    System.out.println("Rotated 180 table:");
    for(int i = 0; i<width;i++){
      for(int j= 0; j<width;j++){
        System.out.printf("%d ",rotimage[i][j]);
      }
      System.out.println();
    }
    System.out.println();
  }
}
